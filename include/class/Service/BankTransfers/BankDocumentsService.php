<?php

namespace Service\BankTransfers;

use Gaufrette\Adapter\Local;
use Gaufrette\Filesystem;
use Psr\Log\LoggerInterface;
use Seranking\Lib\Database\DatabaseConnectionInterface;
use Service\AbstractService;
use Service\NextNumberService;
use Symfony\Component\Stopwatch\Stopwatch;
use System\LogFactory;
use Web\Frontend;

abstract class BankDocumentsService extends AbstractService
{
    const DOCUMENT_TYPE_INVOICE = 'invoice';
    const DOCUMENT_TYPE_CONTRACT = 'contract';
    const DOCUMENT_TYPE_CERTIFICATE = 'certificate';

    const HARDCOPY_STATUS_RECEIVED = 'received';
    const HARDCOPY_STATUS_SENT = 'sent';

    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var NextNumberService
     */
    protected $nextNumberService;

    public function __construct(DatabaseConnectionInterface $db = null)
    {
        parent::__construct($db);
        $adapter = new Local(FILES_PATH.'bank_documents/', true);
        $this->storage = new GaufretteStorage(new Filesystem($adapter));
        $this->logger = LogFactory::create('bank_transfers');
        $this->nextNumberService = new NextNumberService($db);
    }

    /**
     * @param array $data
     * @return string
     */
    public function generatePdf(array $data)
    {
        $timer = new Stopwatch();
        $timer->start('render');

        $tpl = $this->getTemplate();
        if (!file_exists(PRJ_ROOT_PATH.'include/module/Site/templates/'.$tpl)) {
            throw new \RuntimeException('Template doesn\'t exists');
        }

        $template = Frontend::getTemplate();
        $template->assign($data);
        $html = $template->fetch($tpl);

        $mpdf = new \mPDF('utf-8', 'A4');
        $mpdf->WriteHTML($html);
        $result = $mpdf->Output(null, 'S');

        $this->logger->debug(sprintf('generatePdf for template %s in sec: %f', $tpl, $timer->stop('render')->getDuration()/1000));

        return $result;
    }

    abstract protected function getTemplate();
}
