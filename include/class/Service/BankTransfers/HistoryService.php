<?php

namespace Service\BankTransfers;

use Seranking\Lib\Database\Expression\Expression;
use Service\AbstractService;
use Web\Auth;

class HistoryService extends AbstractService
{
    const TABLE_NAME = 'seo_bank_transfers_history';

    public function add($invoiceId, $summary)
    {
        $row = [
            'invoice_id' => $invoiceId,
            'account_id' => Auth::getAccountId(),
            'summary' => $summary,
            'created_at' => date('Y-m-d H:i:s'),
        ];
        $this->db->insert(self::TABLE_NAME, $row);
    }

    /**
     * @param $invoiceId
     * @return array
     */
    public function getInvoiceHistory($invoiceId)
    {
        $sql = 'SELECT * FROM :table WHERE `invoice_id`=:invoice_id ORDER BY `created_at` ASC';
        $params = [
            'table' => new Expression(self::TABLE_NAME),
            'invoice_id' => $invoiceId,
        ];
        return $this->db->getRows($sql, $params);
    }
}
