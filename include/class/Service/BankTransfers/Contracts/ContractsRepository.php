<?php

namespace Service\BankTransfers\Contracts;

use Model\BankDocuments\Contract;
use Seranking\Lib\Database\Expression\Expression;
use Service\BankTransfers\BankDocumentsRepository;

class ContractsRepository extends BankDocumentsRepository
{
    const TABLE_NAME = 'seo_bank_transfers_contracts';

    /**
     * @param Contract $contract
     * @return Contract
     */
    public function save(Contract $contract)
    {
        $row = $contract->toArray();

        // разделить на save и update?

        if ($this->alreadyPersisted($contract)) {
            $this->db->update(self::TABLE_NAME, $row, 'id=:id', ['id' => $contract->getId()]);
        } else {
            $row['created_at'] = date('Y-m-d H:i:s');
            $this->db->insert(self::TABLE_NAME, $row);
        }

        return $this->fetchById($contract->getId());
    }

    /**
     * @param $id
     * @return Contract
     */
    public function fetchById($id)
    {
        $sql ='SELECT * FROM :table WHERE id=:id';
        $params = [
            'table' => new Expression(self::TABLE_NAME),
            'id' => $id,
        ];
        $data = $this->db->getRow($sql, $params);

        return Contract::createFromArray($data);
    }

    /**
     * @param ContractFilter|null $filter
     * @return array
     */
    public function fetchAll(ContractFilter $filter = null)
    {
        $sql = 'SELECT * FROM :table';
        $params = [
            'table' => new Expression(self::TABLE_NAME),
        ];

        if ($filter && $filter->getWhereClauses()) {
            $conditions = [];
            foreach ($filter->getWhereClauses() as $key => $value) {
                $conditions[] = $key.'=:'.$key;
                $params[$key] = $value;
            }
            $sql .= ' WHERE '.implode(' AND ', $conditions);
        }

        $sql .= ' ORDER BY created_at DESC';

        return array_map(function ($row) {
            return Contract::createFromArray($row);
        }, $this->db->getRows($sql, $params));
    }

    public function getTableName()
    {
        return self::TABLE_NAME;
    }
}
