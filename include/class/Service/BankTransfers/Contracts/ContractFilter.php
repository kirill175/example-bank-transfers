<?php

namespace Service\BankTransfers\Contracts;

class ContractFilter
{
    /**
     * @var array
     */
    private $whereClauses;

    public function __construct(array $whereClauses)
    {
        $this->whereClauses = $whereClauses;
    }

    public function getWhereClauses()
    {
        return $this->whereClauses;
    }
}
