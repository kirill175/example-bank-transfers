<?php

namespace Service\BankTransfers\Contracts;

use Seranking\Lib\Utils\Helper\Arr;
use Seranking\Lib\Utils\Helper\Date;
use Seranking\Lib\Utils\Helper\Files;
use Model\BankDocuments\Contract;
use Seranking\Lib\Database\DatabaseConnectionInterface;
use Service\Accounts;
use Service\BankTransfers\BankDocumentsService;

class ContractsService extends BankDocumentsService
{
    /**
     * @var ContractsRepository
     */
    private $repository;

    public function __construct(DatabaseConnectionInterface $db = null)
    {
        parent::__construct($db);
        $this->repository = new ContractsRepository($db);
    }

    /**
     * @param $id
     * @return Contract|null
     */
    public function fetchById($id)
    {
        return $this->repository->fetchById($id);
    }

    /**
     * @param array $input
     * @return Contract
     */
    public function create(array $input)
    {
        $input['id'] = $this->generateId();

        $number = Arr::get($input, 'number');
        if (!$number) {
            $input['number'] = $this->generateNumber();
        }

        $date = Arr::get($input, 'date');
        if (!$date) {
            $input['date'] = date('d.m.Y');
        }

        $country = Arr::get($input, 'company_country');
        if (!$country) {
            $input['company_country'] = Contract::getDefaultCountry();
        }

        $contract = Contract::createFromArray($input);

        $this->generateAndStorePdf($contract);

        $contract = $this->repository->save($contract);

        return $contract;
    }

    public function generateAndStorePdf(Contract $contract)
    {
        $data = $contract->toArray();
        $data['date_str'] = Date::formatWithMonthName('«%s» %s %s года', $data['date']);
        $fileContent = $this->generatePdf($data);

        $fileName = $this->getFileNameForStorage($contract);

        $this->storage->put($fileName, $fileContent);
    }

    /**
     * @param Contract $contract
     * @return array
     */
    public function getDownloadFile(Contract $contract)
    {
        return [
            'filename' => $this->getFileNameForUser($contract),
            'content' => $this->storage->get($this->getFileNameForStorage($contract)),
        ];
    }

    /**
     * @param Contract $contract
     * @return Accounts
     */
    public function getAccount(Contract $contract)
    {
        $accountId = $contract->getAccountId();
        return Accounts::getModelInstance($accountId);
    }

    /**
     * @param $accountId
     * @return array
     */
    public function findContractsByAccountId($accountId)
    {
        $filter = new ContractFilter(['account_id' => $accountId]);
        return $this->repository->fetchAll($filter);
    }

    public function setHardcopyStatus(Contract $contract, $status)
    {
        switch ($status) {
            case BankDocumentsService::HARDCOPY_STATUS_RECEIVED:
                $contract->setIsHardcopyReceived(1);
                break;
            case BankDocumentsService::HARDCOPY_STATUS_SENT:
                $contract->setIsHardcopySent(1);
                break;
            default:
                throw new \InvalidArgumentException('Invalid hard copy status '.$status);
        }
        $contract = $this->repository->save($contract);

        return $contract;
    }

    private function getFileNameForStorage(Contract $contract)
    {
        return 'contract_'.$contract->getId().'.pdf';
    }

    public function getFileNameForUser(Contract $contract)
    {
        return Files::fixSafeFilename('Договор_N'.$contract->getNumber().'_от_'.$contract->getDate().'.pdf');
    }

    private function generateId()
    {
        return $this->nextNumberService->getNextNumber('bank_transfers:contract:id');
    }

    private function generateNumber()
    {
        return $this->nextNumberService->getNextNumber('bank_transfers:contract:number').'/'.date('m-Y');
    }

    /**
     * @return string
     */
    protected function getTemplate()
    {
        return 'bank_transfers/contract.tpl';
    }
}
