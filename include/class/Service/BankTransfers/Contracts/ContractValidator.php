<?php

namespace Service\BankTransfers\Contracts;

use Database\DatabaseFactory;
use Model\BankDocuments\Contract;
use Service\Accounts;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class ContractValidator
{
    /**
     * @var ContractValidator
     */
    protected static $instance;

    protected function __construct()
    {
    }

    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    /**
     * @return Assert\Collection
     */
    public function createSuperadminValidateConstraint()
    {
        $options = $this->getCommonValidationRules();

        $options = array_merge($options, [
            'account_id' => [
                new Assert\Callback([$this, 'checkAccountExists']),
            ],
            'number' => [
                new Assert\NotBlank(),
                new Assert\Callback([$this, 'checkNumberNotExists']),
            ],
            'date' => [
                new Assert\NotBlank(),
            ],
        ]);

        $constraint = new Assert\Collection([
            'fields' => $options,
            'allowExtraFields' => true,
        ]);

        return $constraint;
    }

    /**
     * @param null $country
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getCommonValidationRules($country = null)
    {
        if (is_null($country)) {
            $country = Contract::COUNTRY_RU;
        }

        switch ($country) {
            case Contract::COUNTRY_RU:
                $options = [
                    'company_inn' => [
                        new Assert\NotBlank(),
                    ],
                    'company_kpp' => [
                        new Assert\NotBlank(),
                        new Assert\Callback([$this, 'validateKpp']),
                    ],
                    'company_ogrn' => [
                        new Assert\NotBlank(),
                        new Assert\Length([
                            'min' => 13,
                            'max' => 15,
                            'minMessage' => 'Минимальный размер поля ОГРН/ОГРНИП — 13 знаков',
                            'maxMessage' => 'Максимальный размер поля ОГРН/ОГРНИП — 15 знаков',
                        ]), // https://ru.wikipedia.org/wiki/Основной_государственный_регистрационный_номер, https://ru.wikipedia.org/wiki/Основной_государственный_регистрационный_номер_индивидуального_предпринимателя
                    ],
                    'company_bank_bic' => [
                        new Assert\NotBlank(),
                        new Assert\Callback([$this, 'checkRuBicExists']),
                    ],
                    'company_bank_name' => [
                        new Assert\NotBlank(),
                    ],
                    'company_bank_correspondent_account' => [
                    ],
                ];
                break;
            case Contract::COUNTRY_BY:
                $options = [
                    'company_inn' => [
                        new Assert\NotBlank(),
                        new Assert\Length([
                            'min' => 9,
                            'max' => 9,
                            'exactMessage' => 'Размер поля УНП — 9 знаков',
                        ]),
                    ],
                    'company_bank_bic' => [
                        new Assert\NotBlank(),
                    ],
                    'company_bank_name' => [
                        new Assert\NotBlank(),
                    ],
                    'company_bank_correspondent_account' => [
                    ],
                ];
                break;
            default:
                throw new \InvalidArgumentException('Invalid country '.$country);
        }

        return array_merge($options, [
            'company_name' => [
                new Assert\NotBlank(),
            ],
            'company_bank_account' => [
                new Assert\NotBlank(),
            ],
            'company_representative_post' => [
            ],
            'company_representative_surname' => [
                new Assert\NotBlank(),
            ],
            'company_representative_name' => [
                new Assert\NotBlank(),
            ],
            'company_representative_basis' => [
                new Assert\NotBlank(),
            ],
            'company_legal_address' => [
                new Assert\NotBlank(),
            ],
            'company_postal_code' => [
                new Assert\NotBlank(),
            ],
            'company_postal_address' => [
                new Assert\NotBlank(),
            ],
        ]);
    }

    public function checkAccountExists($accountId, ExecutionContextInterface $context)
    {
        $account = Accounts::getModelInstance($accountId);
        if (!$account->getId()) {
            $context->addViolation('account not exists');
        }
    }

    public function checkRuBicExists($bic, ExecutionContextInterface $context)
    {
        $sql = 'SELECT COUNT(*) FROM `seo_ru_banks_reference` WHERE `bic`=:bic';
        $count = DatabaseFactory::getMainDb()->getField($sql, ['bic' => $bic]);
        if (!$count) {
            $context->addViolation('БИК не найден в справочнике');
        }
    }

    public function checkNumberNotExists($number, ExecutionContextInterface $context)
    {
        $db = DatabaseFactory::getMainDb();
        $sql = 'SELECT COUNT(*) FROM seo_bank_transfers_contracts WHERE `number` = :number';
        $count = $db->getField($sql, ['number' => $number]);
        if ($count){
            $context->addViolation('Уже есть договор с номером '.$number);
        }
    }

    public function validateKpp($kpp, ExecutionContextInterface $context)
    {
        // SR-3871
        if ($kpp == '-') {
            return;
        }
        // https://ru.wikipedia.org/wiki/Идентификационный_номер_налогоплательщика#.D0.9A.D0.BE.D0.B4_.D0.BF.D1.80.D0.B8.D1.87.D0.B8.D0.BD.D1.8B_.D0.BF.D0.BE.D1.81.D1.82.D0.B0.D0.BD.D0.BE.D0.B2.D0.BA.D0.B8_.D0.BD.D0.B0_.D1.83.D1.87.D0.B5.D1.82_.28.D0.9A.D0.9F.D0.9F.29
        if (strlen($kpp) == 9) {
            return;
        }
        $context->addViolation('Это значение должно состоять из 9-ти символов или прочерка "-"');
    }
}
