<?php

namespace Service\BankTransfers;

use Gaufrette\Filesystem;

class GaufretteStorage implements StorageInterface
{
    /**
     * @var Filesystem
     */
    private $filesystem;

    public function __construct(Filesystem $filesystem)
    {
        $this->filesystem = $filesystem;
    }

    public function put($path, $content)
    {
        $this->filesystem->write($path, $content);
    }

    public function get($path)
    {
        return $this->filesystem->read($path);
    }
}
