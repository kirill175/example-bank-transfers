<?php

namespace Service\BankTransfers;

interface StorageInterface
{
    public function put($path, $content);

    public function get($path);
}
