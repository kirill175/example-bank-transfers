<?php

namespace Service\BankTransfers;

use Model\BankDocuments\BankDocumentInterface;
use Seranking\Lib\Database\Expression\Expression;
use Service\AbstractService;

abstract class BankDocumentsRepository extends AbstractService
{
    /**
     * @param BankDocumentInterface $document
     * @return bool
     */
    protected function alreadyPersisted(BankDocumentInterface $document)
    {
        $sql = 'SELECT COUNT(*) FROM :table WHERE id=:id';
        $params = [
            'table' => new Expression($this->getTableName()),
            'id'    => $document->getId(),
        ];
        return (bool) $this->db->getField($sql, $params);
    }

    abstract protected function getTableName();
}
