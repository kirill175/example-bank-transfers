<?php

namespace Service\BankTransfers\Invoices;

use Model\BankDocuments\Invoice;
use Seranking\Lib\Database\Expression\ExpressionList;
use Seranking\Lib\Utils\Helper\Sql;
use Service\AbstractService;
use Service\BankTransfers\Contracts\ContractsRepository;

class InvoicesFinder extends AbstractService
{
    const JOIN_ACCOUNTS = 'accounts';

    private $where = [];
    private $params = [];
    private $joins = [];

    public function statuses(array $statuses)
    {
        $this->where['statuses'] = 'status IN (:statuses)';
        $this->params['statuses'] = new ExpressionList($statuses);

        return $this;
    }

    public function query($query)
    {
        $this->joins[] = self::JOIN_ACCOUNTS;
        $this->where['query'] = '(a.account_login LIKE :query OR a.account_email LIKE :query)';
        $this->params['query'] = '%'.$query.'%';

        return $this;
    }

    public function account($accountId)
    {
        $this->joins[] = self::JOIN_ACCOUNTS;
        $this->where['account_id'] = 'a.account_id=:account_id';
        $this->params['account_id'] = $accountId;

        return $this;
    }

    /**
     * @param null $limit
     * @return Invoice[]
     */
    public function find($limit = null)
    {
        $sql = $this->buildSql($limit);

        $rows = $this->db->getRows($sql, $this->params);

        return array_map(function ($row) {
            return Invoice::createFromArray($row);
        }, $rows);
    }

    /**
     * Количество счетов под фильтром
     */
    public function count()
    {
        $sql = $this->buildSql();
        $sql = Sql::convertSqlToCount($sql);

        return $this->db->getField($sql, $this->params);
    }

    private function buildSql($limit = null)
    {
        $sql = 'SELECT i.* FROM '.InvoicesRepository::TABLE_NAME.' AS i';

        $joinMap = $this->joinsMap();
        foreach (array_unique($this->joins) as $joinName) {
            if (isset($joinMap[$joinName])) {
                $join = $joinMap[$joinName];
            } else {
                $join = $joinName;
            }
            $sql .= ' '.$join.' ';
        }

        if ($this->where) {
            $sql .= ' WHERE '.implode($this->where, ' AND ');
        }

        $sql .= ' ORDER BY i.created_at DESC';

        if ($limit) {
            $sql .= ' limit :limit';
            $this->params['limit'] = $limit;
        }

        return $sql;
    }

    private function joinsMap()
    {
        return [
            self::JOIN_ACCOUNTS => 'JOIN '.ContractsRepository::TABLE_NAME.' as c ON c.id=i.contract_id
                JOIN seo_account as a ON a.account_id=c.account_id',
        ];
    }
}
