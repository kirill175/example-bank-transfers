<?php

namespace Service\BankTransfers\Invoices;

class InvoiceFilter
{
    const OPERATOR_EQ = 'eq';
    const OPERATOR_LIKE = 'like';

    /**
     * @var array
     */
    private $whereClauses;

    public function addWhereClause($property, $operator, $value)
    {
        $this->whereClauses[] = [
            'property' => $property,
            'operator' => $operator,
            'value' => $value,
        ];
    }

    public function getWhereSql()
    {
        $clauses = [];
        foreach ($this->whereClauses as $where) {
            $clause = $where['property'];
            switch ($where['operator']) {
                case self::OPERATOR_EQ:
                    $clause .= '=';
            }
            $clauses[] = '';
        }
    }

    public function getWhereClauses()
    {
        return $this->whereClauses;
    }
}
