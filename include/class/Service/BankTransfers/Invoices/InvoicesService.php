<?php

namespace Service\BankTransfers\Invoices;

use EmailLayout\BankTransfersLayout;
use Gateway\CouponsGateway;
use Seranking\Lib\Utils\Helper\Arr;
use Helper\Currency;
use Seranking\Lib\Utils\Helper\Date;
use Helper\Email;
use Seranking\Lib\Utils\Helper\Files;
use Model\BankDocuments\Invoice;
use Model\Coupons\DiscountCoupon;
use Seranking\Lib\Database\DatabaseConnectionInterface;
use Service\Accounts;
use Service\BankTransfers\BankDocumentsService;
use Service\BankTransfers\Certificates\CertificatesService;
use Service\BankTransfers\Contracts\ContractsService;
use Service\BankTransfers\HistoryService;
use Service\Coupons;
use Service\Licenses;
use Service\Orders;

class InvoicesService extends BankDocumentsService
{
    const MAILING_CODE_BALANCE_CREATED = 'bank_transfers_invoice_balance';
    const MAILING_CODE_SUBSCRIPTION_CREATED = 'bank_transfers_invoice_subscription';
    const MAILING_CODE_BALANCE_PAID = 'bank_transfers_invoice_balance_paid';
    const MAILING_CODE_SUBSCRIPTION_PAID = 'bank_transfers_invoice_subscription_paid';

    const STATUS_CREATED = 'created';
    const STATUS_PAID = 'paid';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_DONE = 'done';

    /**
     * @var ContractsService
     */
    private $contractsService;

    /**
     * @var CertificatesService
     */
    private $certificatesService;

    /**
     * @var InvoicesRepository
     */
    private $repository;

    /**
     * @var Orders
     */
    private $ordersService;

    /**
     * @var HistoryService
     */
    protected $historyService;

    public function __construct(DatabaseConnectionInterface $db = null)
    {
        parent::__construct($db);
        $this->contractsService = ContractsService::getInstance();
        $this->certificatesService = CertificatesService::getInstance();
        $this->repository = new InvoicesRepository($db);
        $this->ordersService = Orders::getInstance();
        $this->historyService = new HistoryService($db);
    }

    /**
     * @param $id
     * @return Invoice|null
     */
    public function fetchById($id)
    {
        return $this->repository->fetchById($id);
    }

    /**
     * @param InvoiceFilter|null $filter
     * @return array
     */
    public function fetchAll(InvoiceFilter $filter = null)
    {
        return $this->repository->fetchAll($filter);
    }

    /**
     * @param array $input
     * @return Invoice
     */
    public function create(array $input)
    {
        $invoiceId = $this->generateId();

        $isNewContract = false;
        $contractId = Arr::get($input, 'contract_id', 0);
        $contract = $this->contractsService->fetchById($contractId);
        if (!$contract) {
            $isNewContract = true;
            $contract = $this->contractsService->create($input);
            $this->historyService->add($invoiceId, 'Создан новый договор');
        } else {
            $this->historyService->add($invoiceId, 'Использован существующий договор');
        }

        $input['id'] = $invoiceId;
        $input['number'] = $this->generateNumber();
        $input['contract_id'] = $contract->getId();
        $input['is_new_contract'] = $isNewContract;
        $input['status'] = self::STATUS_CREATED;
        $input['date'] = date('d.m.Y');
        if ($input['transaction_type'] == Invoice::TRANSACTION_TYPE_BALANCE) {
            $input['subscription_package'] = null;
            $input['subscription_package_name'] = null;
            $input['subscription_check_frequency'] = null;
            $input['subscription_months'] = null;
        } else {
            $input['subscription_package_name'] = Licenses::getPackageTitle($input['subscription_package'], \I18n::LANG_RUSSIAN);
        }

        $invoice = Invoice::createFromArray($input);

        $this->generateAndStorePdf($invoice);

        $invoice = $this->repository->save($invoice);
        $this->historyService->add($invoiceId, sprintf('Создан счет'));

        $this->sendNotificationCreated($invoice, $isNewContract);

        return $invoice;
    }

    public function generateAndStorePdf(Invoice $invoice)
    {
        $invoiceData = $invoice->toArray();
        $invoiceData['date_str'] = Date::formatWithMonthName('«%s» %s %s года', $invoiceData['date']);
        $data = [
            'invoice' => $invoiceData,
            'contract' => $this->getContract($invoice)->toArray(),
            'account' => $this->getAccount($invoice)->getAccountData(),
        ];

        /*
         * Нам нужны два варианта счета - с печатью и без.
         * По электронной почте клиенту отправляем счет с печатью.
         * Затем распечатываем pdf без печати, проставляем печать, подписываем
         * и отправляем клиенту оригинал.
         */
        $withStamp = false;
        $data['with_stamp'] = $withStamp;
        $fileContent = $this->generatePdf($data);
        $fileName = $this->getFileNameForStorage($invoice, $withStamp);
        $this->storage->put($fileName, $fileContent);

        $withStamp = true;
        $data['with_stamp'] = $withStamp;
        $fileContent = $this->generatePdf($data);
        $fileName = $this->getFileNameForStorage($invoice, $withStamp);
        $this->storage->put($fileName, $fileContent);
    }

    /**
     * @param Invoice $invoice
     * @param string $type
     * @param bool $withStamp
     * @return array
     * @throws \InvalidArgumentException
     */
    public function getDownloadFile(Invoice $invoice, $type = BankDocumentsService::DOCUMENT_TYPE_INVOICE, $withStamp = true)
    {
        switch ($type) {
            case BankDocumentsService::DOCUMENT_TYPE_INVOICE:
                return [
                    'filename' => $this->getFileNameForUser($invoice),
                    'content' => $this->storage->get($this->getFileNameForStorage($invoice, $withStamp)),
                ];
            case BankDocumentsService::DOCUMENT_TYPE_CONTRACT:
                $contract = $this->getContract($invoice);
                return $this->contractsService->getDownloadFile($contract);
            case BankDocumentsService::DOCUMENT_TYPE_CERTIFICATE:
                $certificate = $this->getCertificate($invoice);
                return $this->certificatesService->getDownloadFile($certificate);
            default:
                throw new \InvalidArgumentException('Unknown document type '.$type);
        }
    }

    private function sendNotificationCreated(Invoice $invoice, $isNewContract)
    {
        switch ($invoice->getTransactionType()) {
            case Invoice::TRANSACTION_TYPE_BALANCE:
                $template = BankTransfersLayout::factory(self::MAILING_CODE_BALANCE_CREATED, \I18n::LANG_RUSSIAN);
                break;
            case Invoice::TRANSACTION_TYPE_SUBSCRIPTION:
                $template = BankTransfersLayout::factory(self::MAILING_CODE_SUBSCRIPTION_CREATED, \I18n::LANG_RUSSIAN);
                break;
            default:
                throw new \InvalidArgumentException('Unknown transaction type '.$invoice->getTransactionType());
        }
        $template->setInvoice($invoice);
        $template->setReceiverAccount($this->getAccount($invoice)->getId(), false);

        $fileData = $this->getDownloadFile($invoice);
        $filePath = Files::getTempFilePath('', '', 'pdf');
        file_put_contents($filePath, $fileData['content']);
        $template->attachFile($filePath, $fileData['filename']);

        if ($isNewContract) {
            $fileData = $this->getDownloadFile($invoice, BankDocumentsService::DOCUMENT_TYPE_CONTRACT);
            $filePath = Files::getTempFilePath('', '', 'pdf');
            file_put_contents($filePath, $fileData['content']);
            $template->attachFile($filePath, $fileData['filename']);
        }

        $letter = $template->generateLetter();

        $to = $this->getAccountEmail($invoice);
        Email::addEmailToQueue($letter, $to);

        $summary = $isNewContract ? 'Счет и договор отправлены' : 'Счет отправлен';
        $this->historyService->add($invoice->getId(), $summary.' на адрес '.$to);
    }

    /**
     * @param Invoice $invoice
     * @return Invoice
     * @throws \Exception
     */
    public function setStatusPaid(Invoice $invoice)
    {
        if (!$this->validateStatusChange($invoice->getStatus(), self::STATUS_PAID)) {
            throw new \Exception('Invoice can not be paid');
        }

        $this->processPayment($invoice);

        $certificate = $this->certificatesService->create($invoice, $this->getContract($invoice));
        $invoice->setCertificateId($certificate->getId());
        $this->repository->save($invoice);
        $this->historyService->add($invoice->getId(), 'Создан акт оказанных услуг');

        $this->sendNotificationPaid($invoice);

        $invoice->setStatus(self::STATUS_PAID);
        $invoice = $this->repository->save($invoice);

        $this->historyService->add($invoice->getId(), 'Статус счета изменен на "оплачен"');

        return $invoice;
    }

    private function sendNotificationPaid(Invoice $invoice)
    {
        switch ($invoice->getTransactionType()) {
            case Invoice::TRANSACTION_TYPE_BALANCE:
                $template = BankTransfersLayout::factory(self::MAILING_CODE_BALANCE_PAID, \I18n::LANG_RUSSIAN);
                break;
            case Invoice::TRANSACTION_TYPE_SUBSCRIPTION:
                $template = BankTransfersLayout::factory(self::MAILING_CODE_SUBSCRIPTION_PAID, \I18n::LANG_RUSSIAN);
                break;
            default:
                throw new \InvalidArgumentException('Unknown transaction type '.$invoice->getTransactionType());
        }
        $template->setInvoice($invoice);
        $template->setReceiverAccount($this->getAccount($invoice)->getId(), false);

        $fileData = $this->getDownloadFile($invoice, BankDocumentsService::DOCUMENT_TYPE_CERTIFICATE);
        $filePath = Files::getTempFilePath('', '', 'pdf');
        file_put_contents($filePath, $fileData['content']);
        $template->attachFile($filePath, $fileData['filename']);

        if ($invoice->isNewContract()) {
            $fileData = $this->getDownloadFile($invoice, BankDocumentsService::DOCUMENT_TYPE_CONTRACT);
            $filePath = Files::getTempFilePath('', '', 'pdf');
            file_put_contents($filePath, $fileData['content']);
            $template->attachFile($filePath, $fileData['filename']);
        }

        $letter = $template->generateLetter();

        $to = $this->getAccountEmail($invoice);
        Email::addEmailToQueue($letter, $to);

        $this->historyService->add($invoice->getId(), 'Уведомление об оплате отправлено на адрес '.$to);
    }

    /**
     * @param Invoice $invoice
     * @return Invoice
     * @throws \Exception
     */
    public function setStatusCancelled(Invoice $invoice)
    {
        if (!$this->validateStatusChange($invoice->getStatus(), self::STATUS_CANCELLED)) {
            throw new \Exception('Invoice can not be cancelled');
        }

        $invoice->setStatus(self::STATUS_CANCELLED);
        $invoice = $this->repository->save($invoice);

        $this->historyService->add($invoice->getId(), 'Статус счета изменен на "отменен"');

        return $invoice;
    }

    /**
     * Может ли счет быть переведен в статус "Оплачен".
     *
     * @param Invoice $invoice
     * @return bool
     */
    public function canBePaid(Invoice $invoice)
    {
        return $this->validateStatusChange($invoice->getStatus(), self::STATUS_PAID);
    }

    /**
     * Может ли счет быть отменён.
     *
     * @param Invoice $invoice
     * @return bool
     */
    public function canBeCancelled(Invoice $invoice)
    {
        return $this->validateStatusChange($invoice->getStatus(), self::STATUS_CANCELLED);
    }

    private function processPayment(Invoice $invoice)
    {
        $account = $this->getAccount($invoice);

        /** @var DiscountCoupon $coupon */
        $coupon = null;
        $couponCode = $invoice->getCouponCode();
        if ($couponCode) {
            $coupon = Coupons::getInstance()->getCoupon($couponCode, CouponsGateway::TYPE_DISCOUNT);
            if ($coupon === null) {
                $couponCode = null;
            }
        }

        switch ($invoice->getTransactionType()) {
            case Invoice::TRANSACTION_TYPE_BALANCE:
                $amountInUsd = Accounts\AccountBalance::convertToUsd($invoice->getAmount(), Currency::CURRENCY_RUB);

                // из скидки получаем полную сумму пополнения баланса
                if ($coupon !== null) {
                    $amountInUsd = $coupon->calculatePriceFromDiscount($amountInUsd);
                }

                $account->getBalanceService()->refillBalance($amountInUsd);
                $this->historyService->add($invoice->getId(), 'Баланс пополнен на '.$amountInUsd.' USD');
                $orderData = array(
                    'license_id' => null,
                    'account_id' => $account->getId(),
                    'orders_price' => $invoice->getAmount(),
                    'currency' => Currency::CURRENCY_CODE_RUB,
                    'orders_pmethod' => Orders::PAY_SOURCE_BANK_TRANSFER,
                    'is_bonus' => 0,
                    'orders_solution' => Orders::SOLUTION_BALANCE,
                    'promo_code' => $couponCode,
                );
                $this->ordersService->insertOrder($orderData);
                $this->historyService->add($invoice->getId(), 'Добавлен ордер');
                break;
            case Invoice::TRANSACTION_TYPE_SUBSCRIPTION:
                $account->changeCheckFrequency($invoice->getSubscriptionCheckFrequency());
                $this->historyService->add($invoice->getId(), 'Установлена частота проверок '.$invoice->getSubscriptionCheckFrequency());

                $licenseExpirationDate = date('Y-m-d H:i:s', strtotime('+'.$invoice->getSubscriptionMonths().' months'));
                $licenseId = (new Licenses())->createSubscriptionLicense($account->getId(), $invoice->getSubscriptionPackage(), $licenseExpirationDate);
                $this->historyService->add($invoice->getId(), 'Добавлена лицензия');

                $orderData = [
                    'license_id' => $licenseId,
                    'account_id' => $account->getId(),
                    'orders_price' => $invoice->getAmount(),
                    'currency' => Currency::CURRENCY_CODE_RUB,
                    'orders_pmethod' => Orders::PAY_SOURCE_BANK_TRANSFER,
                    'orders_expdate' => $licenseExpirationDate,
                    'orders_package' => $invoice->getSubscriptionPackage(),
                    'orders_months' => $invoice->getSubscriptionMonths(),
                    'is_bonus' => 0,
                    'promo_code' => $couponCode,
                ];
                $this->ordersService->insertOrder($orderData);
                $this->historyService->add($invoice->getId(), 'Добавлен ордер');
                break;

            default:
                throw new \InvalidArgumentException('Unknown transaction type '.$invoice->getTransactionType());
        }
    }

    public function setHardcopyStatus(Invoice $invoice, $documentType, $status)
    {
        switch ($documentType) {
            case BankDocumentsService::DOCUMENT_TYPE_INVOICE:
                switch ($status) {
                    case BankDocumentsService::HARDCOPY_STATUS_SENT:
                        $invoice->setIsHardcopySent(1);
                        $invoice = $this->repository->save($invoice);
                        $this->historyService->add($invoice->getId(), 'Клиенту отправлен подписанный нами счет');
                        break;
                    default:
                        throw new \InvalidArgumentException('Invalid hard copy status '.$status);
                }
                break;
            case BankDocumentsService::DOCUMENT_TYPE_CONTRACT:
                $contract = $this->getContract($invoice);
                $this->contractsService->setHardcopyStatus($contract, $status);
                if ($status == BankDocumentsService::HARDCOPY_STATUS_RECEIVED) {
                    $this->historyService->add($invoice->getId(), 'Получен оригинал договора с подписями и печатями клиента');
                } else {
                    $this->historyService->add($invoice->getId(), 'Клиенту отправлен подписанный нами договор');
                }
                break;
            case BankDocumentsService::DOCUMENT_TYPE_CERTIFICATE:
                $certificate = $this->getCertificate($invoice);
                $this->certificatesService->setHardcopyStatus($certificate, $status);
                if ($status == BankDocumentsService::HARDCOPY_STATUS_RECEIVED) {
                    $this->historyService->add($invoice->getId(), 'Получен оригинал акта с подписями и печатями клиента');
                } else {
                    $this->historyService->add($invoice->getId(), 'Клиенту отправлен подписанный нами акт');
                }
                break;
        }

        return $this->checkIfInvoiceDone($invoice);
    }

    /**
     * Счёт считается исполненным, если он оплачен,
     * получены оргиналы договора и акта с подписями и печатямии клиента
     * и клиенту отправлены исходящие документы: договор, акт и счет.
     *
     * @param Invoice $invoice
     * @return Invoice
     */
    private function checkIfInvoiceDone(Invoice $invoice)
    {
        $contract = $this->getContract($invoice);
        $certificate = $this->getCertificate($invoice);

        if ($invoice->getStatus() == self::STATUS_PAID
            && $contract->isHardcopyReceived() && $contract->isHardcopySent()
            && $invoice->isHardcopySent()
            && $certificate && $certificate->isHardcopyReceived() && $certificate->isHardcopySent()
        ) {
            $invoice->setStatus(self::STATUS_DONE);
            $invoice = $this->repository->save($invoice);
            $this->historyService->add($invoice->getId(), 'Статус счета изменен на "исполнен"');
        }

        return $invoice;
    }

    private function getFileNameForStorage(Invoice $invoice, $withStamp)
    {
        return 'invoice_'.$invoice->getId().'_'.(int)$withStamp.'.pdf';
    }

    public function getFileNameForUser(Invoice $invoice)
    {
        return Files::fixSafeFilename('Счет_N'.$invoice->getNumber().'_от_'.$invoice->getDate().'.pdf');
    }

    /**
     * @param Invoice $invoice
     * @return \Model\BankDocuments\Contract|null
     */
    public function getContract(Invoice $invoice)
    {
        return $this->contractsService->fetchById($invoice->getContractId());
    }

    /**
     * @param Invoice $invoice
     * @return \Model\BankDocuments\Certificate|null
     */
    public function getCertificate(Invoice $invoice)
    {
        return $this->certificatesService->fetchById($invoice->getCertificateId());
    }

    /**
     * @return string
     */
    protected function getTemplate()
    {
        return 'bank_transfers/invoice.tpl';
    }

    /**
     * @param Invoice $invoice
     * @return Accounts
     */
    public function getAccount(Invoice $invoice)
    {
        $contract = $this->getContract($invoice);
        $accountId = $contract->getAccountId();
        return Accounts::getModelInstance($accountId);
    }

    /**
     * Может ли счет в статусе currentStatus быть переведен в статус $newStatus?
     *
     * @param $currentStatus
     * @param $newStatus
     * @return bool
     */
    private function validateStatusChange($currentStatus, $newStatus)
    {
        // allow: created->paid, created->cancelled
        // deny: the rest
        $allowed = [
            self::STATUS_CREATED.self::STATUS_PAID,
            self::STATUS_CREATED.self::STATUS_CANCELLED,
        ];

        if (in_array($currentStatus.$newStatus, $allowed)) {
            return true;
        }

        return false;
    }

    public function getHistory(Invoice $invoice)
    {
        return $this->historyService->getInvoiceHistory($invoice->getId());
    }

    /**
     * @param Invoice $invoice
     * @return string
     */
    private function getAccountEmail(Invoice $invoice)
    {
        $account = $this->getAccount($invoice);
        return $account->getEmail();
    }

    /**
     * Разрешено ли юзеру скачать pdf.
     *
     * @param Invoice $invoice
     * @param Accounts $account
     * @return bool
     */
    public function validateDownload(Invoice $invoice, Accounts $account)
    {
        if ($account->isSuperAdmin()) {
            return true;
        }

        $creator = $this->getAccount($invoice);
        if ($creator->getId() == $account->getId() || $creator->getParentId() == $account->getId()) {
            return true;
        }

        return false;
    }

    private function generateId()
    {
        return $this->nextNumberService->getNextNumber('bank_transfers:invoice:id');
    }

    private function generateNumber()
    {
        return $this->nextNumberService->getNextNumber('bank_transfers:invoice:number');
    }

    /**
     * @param bool $forSuperadmin
     * @return array
     */
    public function getAllowedStatuses($forSuperadmin = false)
    {
        if ($forSuperadmin) {
            return [
                self::STATUS_CREATED,
                self::STATUS_PAID,
                self::STATUS_CANCELLED,
                self::STATUS_DONE,
            ];
        }

        return [
            self::STATUS_CREATED,
            self::STATUS_PAID,
            self::STATUS_DONE,
        ];
    }

    /**
     * @param $query
     * @return array
     */
    public function findBanksByBicPart($query)
    {
        $sql = 'SELECT * FROM `seo_ru_banks_reference` WHERE `bic` LIKE :query LIMIT 20';
        $params = [
            'query' => '%'.$query.'%',
        ];
        return $this->db->getRows($sql, $params);
    }
}
