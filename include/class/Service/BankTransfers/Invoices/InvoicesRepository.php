<?php

namespace Service\BankTransfers\Invoices;

use Model\BankDocuments\Invoice;
use Seranking\Lib\Database\Expression\Expression;
use Service\BankTransfers\BankDocumentsRepository;

class InvoicesRepository extends BankDocumentsRepository
{
    const TABLE_NAME = 'seo_bank_transfers_invoices';

    /**
     * @param Invoice $invoice
     * @return Invoice
     */
    public function save(Invoice $invoice)
    {
        $row = $invoice->toArray();

        // разделить на save и update?

        if ($this->alreadyPersisted($invoice)) {
            $this->db->update(self::TABLE_NAME, $row, 'id=:id', ['id' => $invoice->getId()]);
        } else {
            $row['created_at'] = date('Y-m-d H:i:s');
            $this->db->insert(self::TABLE_NAME, $row);
        }

        return $this->fetchById($invoice->getId());
    }

    /**
     * @param $id
     * @return Invoice
     */
    public function fetchById($id)
    {
        $sql ='SELECT * FROM :table WHERE id=:id';
        $params = [
            'table' => new Expression(self::TABLE_NAME),
            'id' => $id,
        ];
        $data = $this->db->getRow($sql, $params);

        return Invoice::createFromArray($data);
    }

    public function fetchAll(InvoiceFilter $filter = null)
    {
        $sql = 'SELECT * FROM :table';
        $params = [
            'table' => new Expression(self::TABLE_NAME),
        ];

        if ($filter && $filter->getWhereClauses()) {
            $conditions = [];
            foreach ($filter->getWhereClauses() as $key => $value) {
                $conditions[] = $key.'=:'.$key;
                $params[$key] = $value;
            }
            $sql .= ' WHERE '.implode(' AND ', $conditions);
        }

        $sql .= ' ORDER BY created_at DESC';

        return array_map(function ($row) {
            return Invoice::createFromArray($row);
        }, $this->db->getRows($sql, $params));
    }

    protected function getTableName()
    {
        return self::TABLE_NAME;
    }
}
