<?php

namespace Service\BankTransfers\Invoices;

use Database\DatabaseFactory;
use Helper\Arr;
use Model\BankDocuments\Invoice;
use Model\Coupons\DiscountCoupon;
use Service\Accounts;
use Service\BankTransfers\Contracts\ContractValidator;
use Service\Licenses;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class InvoiceValidator
{
    /**
     * @var InvoiceValidator
     */
    protected static $instance;

    protected function __construct()
    {
    }

    public static function getInstance()
    {
        if (!static::$instance) {
            static::$instance = new static;
        }
        return static::$instance;
    }

    /**
     * @param $transactionType
     * @param $isNewContract
     * @param $country
     * @return Assert\Collection
     */
    public function createSuperadminValidateConstraint($transactionType, $isNewContract, $country)
    {
        $options = $this->getCommonValidationConstraint($transactionType, $isNewContract, $country);

        $options = array_merge($options, [
            'account_id' => [
                new Assert\Callback([$this, 'checkAccountExists']),
            ],
        ]);

        $constraint = new Assert\Collection([
            'fields' => $options,
            'allowExtraFields' => true,
        ]);

        return $constraint;
    }

    /**
     * @param $isNewContract
     * @return Assert\Collection
     */
    public function createSubscriptionTransactionValidationConstraint($isNewContract)
    {
        $options = array_merge($this->getCommonValidationConstraint(Invoice::TRANSACTION_TYPE_SUBSCRIPTION, $isNewContract), [
            'subscription_months' => [
                new Assert\Callback([$this, 'checkSubscriptionMonths']),
            ],
        ]);

        $constraint = new Assert\Collection([
            'fields' => $options,
            'allowExtraFields' => true,
        ]);

        return $constraint;
    }

    public function createBalanceTransactionValidationConstraint($isNewContract, DiscountCoupon $coupon = null)
    {
        $minAmount = Invoice::MIN_BALANCE_AMOUNT;
        if ($coupon !== null) {
            $minAmount = $coupon->calculateDiscountPrice($minAmount);
        }

        $options = array_merge($this->getCommonValidationConstraint(Invoice::TRANSACTION_TYPE_BALANCE, $isNewContract), [
            'amount' => [
                new Assert\GreaterThanOrEqual($minAmount),
            ],
        ]);

        $constraint = new Assert\Collection([
            'fields' => $options,
            'allowExtraFields' => true,
        ]);

        return $constraint;
    }

    /**
     * @param $transactionType
     * @param $isNewContract
     * @param null $country
     * @return array
     */
    private function getCommonValidationConstraint($transactionType, $isNewContract, $country = null)
    {
        if (!$isNewContract) {
            return [];
        }

        return ContractValidator::getInstance()->getCommonValidationRules($country);
    }

    public function checkAccountExists($accountId, ExecutionContextInterface $context)
    {
        $account = Accounts::getModelInstance($accountId);
        if (!$account->getId()) {
            $context->addViolation('account not exists');
        }
    }

    public function checkBicExists($bic, ExecutionContextInterface $context)
    {
        $sql = 'SELECT COUNT(*) FROM `seo_ru_banks_reference` WHERE `bic`=:bic';
        $count = DatabaseFactory::getMainDb()->getField($sql, ['bic' => $bic]);
        if (!$count) {
            $context->addViolation('БИК не найден в справочнике');
        }
    }

    /**
     * Оплата по безналу разрешена только для определенных комбинаций тариф + период оплаты.
     * Например, "Персональный" можно оплатить на период от 1 года, "Оптимум" - от 6 месяцев и т.п.
     *
     * Метод проверяет, удовлетворяет ли комбинация тариф/период оплаты лимитам оплаты по безналу.
     *
     * @param $months
     * @param ExecutionContextInterface $context
     */
    public function checkSubscriptionMonths($months, ExecutionContextInterface $context)
    {
        $isValid = false;
        $subscriptionPackage = Arr::get($context->getRoot(), 'subscription_package');
        $limits = Licenses::getSubscriptionsDataForBankTransfers();
        foreach ($limits as $package) {
            if ($package['internal_code'] == $subscriptionPackage) {
                foreach ($package['periods'] as $period) {
                    if ($period['months'] == $months) {
                        $isValid = true;
                    }
                }
            }
        }
        if (!$isValid) {
            $context->addViolation('Параметры подписки не удовлетворяют лимитам на безналичную оплату');
        }
    }
}
