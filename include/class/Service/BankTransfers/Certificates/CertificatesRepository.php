<?php

namespace Service\BankTransfers\Certificates;

use Model\BankDocuments\Certificate;
use Seranking\Lib\Database\Expression\Expression;
use Service\BankTransfers\BankDocumentsRepository;

class CertificatesRepository extends BankDocumentsRepository
{
    const TABLE_NAME = 'seo_bank_transfers_certificates';

    /**
     * @param Certificate $certificate
     * @return Certificate
     */
    public function save(Certificate $certificate)
    {
        $row = $certificate->toArray();

        // разделить на save и update?

        if ($this->alreadyPersisted($certificate)) {
            $this->db->update(self::TABLE_NAME, $row, 'id=:id', ['id' => $certificate->getId()]);
        } else {
            $row['created_at'] = date('Y-m-d H:i:s');
            $this->db->insert(self::TABLE_NAME, $row);
        }

        return $this->fetchById($certificate->getId());
    }

    /**
     * @param $id
     * @return Certificate
     */
    public function fetchById($id)
    {
        $sql ='SELECT * FROM :table WHERE id=:id';
        $params = [
            'table' => new Expression(self::TABLE_NAME),
            'id'=> $id,
        ];
        $data = $this->db->getRow($sql, $params);

        return Certificate::createFromArray($data);
    }

    /**
     * @param CertificateFilter|null $filter
     * @return array
     */
    public function fetchAll(CertificateFilter $filter = null)
    {
        $sql = 'SELECT * FROM :table';
        $params = [
            'table' => new Expression(self::TABLE_NAME),
        ];

        if ($filter && $filter->getWhereClauses()) {
            $conditions = [];
            foreach ($filter->getWhereClauses() as $key => $value) {
                $conditions[] = $key.'=:'.$key;
                $params[$key] = $value;
            }
            $sql .= ' WHERE '.implode(' AND ', $conditions);
        }

        $sql .= ' ORDER BY created_at DESC';

        return array_map(function ($row) {
            return Certificate::createFromArray($row);
        }, $this->db->getRows($sql, $params));
    }

    protected function getTableName()
    {
        return self::TABLE_NAME;
    }
}
