<?php

namespace Service\BankTransfers\Certificates;

use Seranking\Lib\Utils\Helper\Date;
use Seranking\Lib\Utils\Helper\Files;
use Model\BankDocuments\Certificate;
use Model\BankDocuments\Contract;
use Model\BankDocuments\Invoice;
use Seranking\Lib\Database\DatabaseConnectionInterface;
use Seranking\Lib\Utils\Helper\RussianGrammar;
use Service\BankTransfers\BankDocumentsService;

class CertificatesService extends BankDocumentsService
{
    /**
     * @var CertificatesRepository
     */
    private $repository;

    public function __construct(DatabaseConnectionInterface $db = null)
    {
        parent::__construct($db);
        $this->repository = new CertificatesRepository($db);
    }

    /**
     * @param $id
     * @return Certificate|null
     */
    public function fetchById($id)
    {
        return $this->repository->fetchById($id);
    }

    /**
     * @param Invoice $invoice
     * @param Contract $contract
     * @return Certificate|null
     */
    public function create(Invoice $invoice, Contract $contract)
    {
        $input['id'] = $this->generateId();
        $input['number'] = $this->generateNumber();
        $input['date'] = date('d.m.Y');
        $input['service_period_from'] = date('d.m.Y');
        $input['service_period_to'] = date('d.m.Y', strtotime('+'.$invoice->getSubscriptionMonths().' months'));
        $certificate = Certificate::createFromArray($input);

        $this->generateAndStorePdf($certificate, $invoice, $contract);

        $certificate = $this->repository->save($certificate);

        return $certificate;
    }

    public function generateAndStorePdf(Certificate $certificate, Invoice $invoice, Contract $contract)
    {
        $certificateData = $certificate->toArray();
        $certificateData['date_str'] = Date::formatWithMonthName('«%s» %s %s года', $certificateData['date']);
        $certificateData['service_period_from_str'] = Date::formatWithMonthName('%s %s %s года', $certificateData['service_period_from']);
        $certificateData['service_period_to_str'] = Date::formatWithMonthName('%s %s %s года', $certificateData['service_period_to']);

        $invoiceData = $invoice->toArray();
        $invoiceData['amount_str'] = RussianGrammar::numberToText($invoice->getAmount());

        $contractData = $contract->toArray();
        $contractData['date_str'] = Date::formatWithMonthName('«%s» %s %s года', $contractData['date']);
        $data = [
            'certificate' => $certificateData,
            'invoice' => $invoiceData,
            'contract' => $contractData,
        ];
        $fileContent = $this->generatePdf($data);

        $fileName = $this->getFileNameForStorage($certificate);

        $this->storage->put($fileName, $fileContent);
    }

    /**
     * @param Certificate $certificate
     * @return array
     */
    public function getDownloadFile(Certificate $certificate)
    {
        return [
            'filename' => $this->getFileNameForUser($certificate),
            'content' => $this->storage->get($this->getFileNameForStorage($certificate)),
        ];
    }

    private function getFileNameForStorage(Certificate $certificate)
    {
        return 'certificate_'.$certificate->getId().'.pdf';
    }

    public function getFileNameForUser(Certificate $certificate)
    {
        return Files::fixSafeFilename('Акт_N'.$certificate->getNumber().'_от_'.$certificate->getDate().'.pdf');
    }

    /**
     * @param Certificate $certificate
     * @param $status
     * @return Certificate
     * @throws \InvalidArgumentException
     */
    public function setHardcopyStatus(Certificate $certificate, $status)
    {
        switch ($status) {
            case BankDocumentsService::HARDCOPY_STATUS_RECEIVED:
                $certificate->setIsHardcopyReceived(1);
                break;
            case BankDocumentsService::HARDCOPY_STATUS_SENT:
                $certificate->setIsHardcopySent(1);
                break;
            default:
                throw new \InvalidArgumentException('Invalid hard copy status '.$status);
        }

        return $this->repository->save($certificate);
    }

    private function generateId()
    {
        return $this->nextNumberService->getNextNumber('bank_transfers:certificate:id');
    }

    private function generateNumber()
    {
        return $this->nextNumberService->getNextNumber('bank_transfers:certificate:number');
    }

    /**
     * @return string
     */
    protected function getTemplate()
    {
        return 'bank_transfers/certificate.tpl';
    }
}
